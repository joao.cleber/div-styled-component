# Div Styled Component - Documentação
---

Componente desenvolvido para que seja possivel criar um elemento pai personalizado para os elementos filhos, e para que seja possivel estar alterando alguns estilos dos elementos filhos por meio do elemento pai.

##### Propriedades

- **ConfigTitle**
    - **tagTitle**
    - **colorTitle**
    - **sizeTitle**
    - **paddingTitle**
<br/>

- **ConfigText** Nota (Essa propriedade é um array, sendo assim é possível definir mais de uma propriedade para mais de uma tag)
    - **tagText**
    - **colorText**
    - **sizeText**
    - **paddingText**
<br/>

- **ConfigDiv** Nota (Essa propriedade é um array, sendo assim é possível definir mais de uma propriedade para mais de uma tag)
    - **isVisible**
    - **colorDiv**
    - **marginDiv**
    - **paddingDiv**
    - **widthDiv**
    - **heightDiv**
<br/>

---
##### Descrição das propriedades

- **ConfigTitle** : Contém as propriedades para configuração dos estilos das tags: h1, h2, h3, h4, h5 e h6.
    - **tagTitle** : tag que deverá conter o estilos informados. ex: ("h1")
    - **colorTitle** : cor do título que deverá ser apresentado. ex: ("red")
    - **sizeTitle** : tamanho da fonte do título que deverá ser apresentado. ex("20px")
    - **paddingTitle** : padding do título. ex("10px 10px 10px 10px")
<br/>

- **ConfigText** : Contém as propriedades para configuração dos estilos das tags: p, span, input, a, div, etc....
    - **tagText** : tag que deverá conter o estilos informados. ex: ("p")
    - **colorText** : cor do título que deverá ser apresentado. ex: ("red")
    - **sizeText** : tamanho da fonte do título que deverá ser apresentado. ex("20px")
    - **paddingText** : padding do título. ex("10px 10px 10px 10px")
<br/>

- **ConfigDiv** : Contém as propriedades para configuração da própria div
    - **isVisible** : tag para informar se a div deverá ser apresentada ou não. ex: (true ou false)
    - **colorDiv** : cor de fundo da div que deverá ser apresentado. ex: ("red")
    - **marginDiv** : margem que a div deverá ter. ex: ("10px 10px 10px 10px")
    - **paddingDiv** : padding que a div deverá ter. ex: ("10px 10px 10px 10px")
    - **widthDiv** : largura total da div. ex("20px")
    - **heightDiv** : altura total da div. ex("10px")

---
#### Adicionando uma cor personalizada

Para adicionar uma nova cor personalizada basta adicionar um novo css dentro do arquivo **DivStyled.css** e incluir o nome da classe no enum **PersonalizedColor** dentro do arquivo **enum-div-styled.tsx**.

Caso a cor já esteja definida dentro do próprio css, basta adicionar o nome dessa cor dentro do enum **DefaultColorList** dentro do arquivo **enum-div-styled.tsx**.

---
#### Adicionando uma fonte personalizada

Para adicionar uma fonte em expecifico basta no enum **FontList** no arquivo **enum-div-styled.tsx** incluir a fonte desejada.

Caso queira uma sequencia de fontes novas basta utilizar a função **includeAllDefaultFonts()** para criar uma sequencia de fontes prontas para o uso.

---
#### Adicionando novas tags para personalização

Para adicionar uma nova tag para especificação de estilo basta ir até o enum **TagTitlesList** ou **TagTextsList** e adicionar a tag especifica.

Atenção: adicionar a tag conforme necessário e conforme a proprieade.

---
#### Nota

Sempre incluir a propriedade isVisible da propriedade pai ConfigDiv, pois sem o mesmo será necessário estar acionando diretamente pelo site editor da vtex.

---

### Div Styled Component - Documentation
---

Component developed to make it possible to create a custom parent element for the child elements, and to make it possible to be changing some styles of the child elements through the parent element.
<br/>

##### Properties

- **ConfigTitle** Note (This property is an array, so it is possible to define more than one property for more than one tag)
    - **tagTitle**
    - **colorTitle**
    - **sizeTitle**
    - **paddingTitle**
<br/>

- **ConfigText** Note (This property is an array, so it is possible to define more than one property for more than one tag)
    - **tagText**
    - **colorText**
    - **sizeText**
    - **paddingText**
<br/>

- **ConfigDiv**
    - **isVisible**
    - **colorDiv**
    - **marginDiv**
    - **paddingDiv**
    - **widthDiv**
    - **heightDiv**
<br/>

---
##### Description of the properties

- **ConfigTitle** : Contains the properties for configuring the styles of tags: h1, h2, h3, h4, h5 and h6.
    - **tagTitle** : tag that must contain the given style. e.g: ("h1")
    - **colorTitle**: color of the title that should be displayed. ex: ("red")
    - **sizeTitle**: font size of the title to be shown. ex("20px")
    - **paddingTitle** : padding of the title. ex("10px 10px 10px 10px 10px")
<br/>

- **ConfigText** : contains the properties for configuring the styles of tags: p, span, input, a, div, etc....
    - **tagText** : tag that should contain the styles entered. e.g: ("p")
    - **colorText**: color of the title that should be displayed. eg: ("red")
    - **sizeText**: font size of the title that should be shown. ex("20px")
    - **paddingText** : padding of the title. ex("10px 10px 10px 10px")
<br/>

- **ConfigDiv** : contains the properties for configuring the div itself
    - **isVisible** : tag to inform whether the div should be displayed or not. ex: (true or false)
    - **colorDiv** : background color of the div that should be displayed. ex: ("red")
    - **marginDiv** : margin that the div should have. ex: ("10px 10px 10px 10px 10px")
    - **paddingDiv** : padding the div should have. eg: ("10px 10px 10px 10px 10px")
    - **widthDiv** : total width of the div. ex("20px")
    - **heightDiv** : total height of the div. ex("10px")

---
#### Adding a custom color

To add a new custom color simply add a new css inside the **DivStyled.css** file and include the class name in the **PersonalizedColor** enum inside the **enum-div-styled.tsx** file.

If the color is already defined within the css itself, simply add the name of that color to the **DefaultColorList** enum within the **enum-div-styled.tsx** file.

---
#### Adding a custom font

To add a specific font, simply enter the desired font into the **FontList** enum in the **enum-div-styled.tsx** file.

If you want a sequence of new fonts just use the function **includeAllDefaultFonts()** to create a sequence of ready-to-use fonts.

---
#### Adding new customization tags

To add a new tag for style specification just go to the enum **TagTitlesList** or **TagTextsList** and add the specified tag.

### Note

Add the tag as needed and as the property allows.