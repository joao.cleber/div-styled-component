import React from 'react';
import { render, screen } from '@vtex/test-tools/react';
import DivStyled from '../../DivStyled';

import { ConfigTitle, ConfigText, ConfigDiv } from './types/types-div-styled';
import { createDynamicClass, includeAllDefaultFonts, includeAllValues, searchColor, setColorPsStyle, setConfigDivStyle, setConfigTextStyle, setConfigTitleStyle, setNaN } from './utils/util-div-styled';

describe("DivStyled Component", () => {
    it("Verifica se o componente está rodando normalmente", () => {
        render(
            <DivStyled configDiv={{isVisible: true}}>
                <p>Hello World!</p>
            </DivStyled>
        );

        const result = screen.getByTestId("div-test");

        expect(result).toBeDefined();
        expect(result).toHaveTextContent("Hello World!");
    });

    it("Verifica se está se comportando normalmente com todos parametros setados", () => {
        render(
            <DivStyled configDiv={{isVisible: true, colorDiv: "red", heightDiv: "100px", widthDiv: "100px", marginDiv: "10px 10px 50px 50px", paddingDiv: "20px 20px 60px 60px"}}
                       configText={[{colorText: "blue", paddingText: "10px 10px 15px 15px", sizeText: "22px", tagText: "p"}]}
                       configTitle={[{colorTitle: "green", paddingTitle: "10px 10px 15px 15px", sizeTitle: "22px", tagTitle: "h1"}]}>
                <p>Hello World!</p>
            </DivStyled>
        );

        const result = screen.getByTestId("div-test");

        expect(result).toBeDefined();
        expect(result).toHaveTextContent("Hello World!");
    });

    it("Verifica se a classe de cor personalizada está sendo importada normalmente", () => {
        render(<DivStyled configDiv={{isVisible: true, colorDiv: "ps-back-gray-2"}} configTitle={[{colorTitle: "ps-bright-blue"}]}></DivStyled>);

        const result = screen.getByTestId("div-test");

        const result2 = setColorPsStyle([{colorTitle: "ps-bright-blue" }], "configTitle", [{configTitlePs: true}]);

        expect(result).toBeDefined();
        
        expect(result2).toEqual("ps-bright-blue ");
        expect(result).toHaveClass("ps-bright-blue");
        expect(result).toHaveClass("ps-back-gray-2");
    });
});

describe("Functions for Component", () => {
    it("Verifica se a função setConfigTitleStyle está rodando corretamente", () => {
        const configTitle: ConfigTitle = {
            colorTitle: "silver", 
            paddingTitle: "10px 10px 15px 15px", 
            sizeTitle: "22px", 
            tagTitle: "h1"
        };
        const result1 = setConfigTitleStyle([configTitle]);

        render(<DivStyled configDiv={{isVisible: true}} configTitle={[configTitle]}><h1>Hello World!</h1></DivStyled>);

        const result2 = screen.getByTestId("div-test");

        expect(result1).toEqual([{
            "style": {
                "color": "silver",
                "fontSize": "22px",
                "padding": "10px 10px 15px 15px"
            },
            "configTitlePs": false
        }]);
        expect(result2).toBeDefined();
        expect(result2).toHaveTextContent("Hello World!");
    });

    it("Verifica se a função setConfigTextStyle está rodando corretamente", () => {
        const configText: ConfigText = {
            colorText: "blue", 
            paddingText: "10px 10px 15px 15px", 
            sizeText: "22px", 
            tagText: "p"
        };
        const result1 = setConfigTextStyle([configText]);

        render(<DivStyled configDiv={{isVisible: true}} configText={[configText]}><p>Hello World!</p></DivStyled>);

        const result2 = screen.getByTestId("div-test");

        expect(result1).toEqual([{
            "style": {
                "color": "blue",
                "fontSize": "22px",
                "padding": "10px 10px 15px 15px"
            },
            "configTextPs": false
        }]);
        expect(result2).toBeDefined();
        expect(result2).toHaveTextContent("Hello World!");
    });

    it("Verifica se a função setConfigDivStyle está rodando corretamente", () => {
        const configDiv: ConfigDiv = {
            isVisible: true, 
            colorDiv: "red", 
            heightDiv: "100px", 
            widthDiv: "100px", 
            marginDiv: "10px 10px 50px 50px", 
            paddingDiv: "20px 20px 60px 60px"
        };
        const result1 = setConfigDivStyle(configDiv);

        render(<DivStyled configDiv={configDiv}><h1>Hello World!</h1></DivStyled>);

        const result2 = screen.getByTestId("div-test");

        expect(result1).toEqual({
            "style": {
                "backgroundColor": "red",
                "margin": "10px 10px 50px 50px",
                "padding": "20px 20px 60px 60px",
                "width": "100px",
                "height": "100px"
            },
            "configDivPs": false
        });
        expect(result2).toBeDefined();
        expect(result2).toHaveTextContent("Hello World!");
    });

    it("Verifica se a função setNaN está rodando corretamente", () => {
        const result1 = setNaN(undefined);
        const result2 = setNaN("red");
        expect(result1).toEqual("");
        expect(result2).toEqual("red");
    });

    it("Verifica se a função searchColor está rodando corretamente", () => {
        const result1 = searchColor("red");
        const result2 = searchColor("testedecorerrada");
        expect(result1).toEqual(true);
        expect(result2).toEqual(false);
    });

    it("Verifica se a função includeAllValues está rodando corretamente", () => {
        let arrayOld = [1,2,3,4];
        let arrayNew = [5,6,7,8];
        const result = includeAllValues(arrayOld, arrayNew);
        expect(result).toEqual([1,2,3,4,5,6,7,8]);
    });

    it("Verifica se a função includeAllDefaultFonts está rodando corretamente", () => {
        let arrayOld = [""];
        const result = includeAllDefaultFonts(arrayOld, 1, 10, 1);
        expect(result).toEqual(["","1px","2px","3px","4px","5px","6px","7px","8px","9px","10px"]);
    });

    it("Verifica se a função createDynamicClass está rodando corretamente", () => {
        const configDiv: ConfigDiv = {
            isVisible: true, 
            colorDiv: "red", 
            heightDiv: "100px", 
            widthDiv: "100px", 
            marginDiv: "10px 10px 50px 50px", 
            paddingDiv: "20px 20px 60px 60px"
        };
        const dynamicClass = setConfigDivStyle(configDiv);
        const result = createDynamicClass("testClass", dynamicClass.style)

        let css = "\n.testClass";
        css += " {";
        css += "\n    color:"            + "''" + ";";
        css += "\n    background-color:" + "red" + ";";
        css += "\n    padding:"          + "20px 20px 60px 60px" + ";";
        css += "\n    margin:"           + "10px 10px 50px 50px" + ";";
        css += "\n    width:"            + "100px" + ";";
        css += "\n    height:"           + "100px" + ";";
        css += "\n    font-size:"        + "''" + ";";
        css += "\n }";

        expect(result).toEqual(css);
    });
});