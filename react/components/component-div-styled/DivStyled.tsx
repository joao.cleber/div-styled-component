import React from "react";
import { DivProps } from "./interfaces/interface-div-styled";
import { createAllDynamicClasses, createDynamicClass, setColorPsStyle, setConfigDivStyle, setConfigTextStyle, setConfigTitleStyle, setNaN } from "./utils/util-div-styled";
import { DivStyleSchema } from "./schemas/schema-styleddiv";
import style from './DivStyled.css';

const DivStyled: StorefrontFunctionComponent<DivProps> = (props) => {

    //Verifica se a div deverá ser mostrada ou não, caso não precise será retornado null
    if(!props.configDiv?.isVisible)
        return null;

    //Separa os parametros do configTitle passados pela prop para configurar os demais dados do componente
    const configTitle = setConfigTitleStyle(props.configTitle);
    //Captura o nome da classe da cor caso ela seja personalizada, caso contrario não sera definida uma classe para cor e sim o style
    const colorTitlePs = setColorPsStyle(props.configTitle, "configTitle", configTitle); //configTitle.configTitlePs ? style[setNaN(props.configTitle?.colorTitle)] + ";" : "";
    
    //Separa os parametros do configText passados pela prop para configurar os demais dados do componente
    const configText = setConfigTextStyle(props.configText);
    //Captura o nome da classe da cor caso ela seja personalizada, caso contrario não sera definida uma classe para cor e sim o style
    const colorTextPs = setColorPsStyle(props.configText, "configText", configText); //configText.configTextPs ? style[setNaN(props.configText?.colorText)] + ";" : "";

    //Separa os parametros do configDiv passados pela prop para configurar os demais dados do componente
    const configDiv = setConfigDivStyle(props.configDiv);
    //Captura o nome da classe da cor caso ela seja personalizada, caso contrario não sera definida uma classe para cor e sim o style
    const colorDivPs = configDiv.configDivPs ? style[setNaN(props.configDiv?.colorDiv)] + ";" : "";

    //Cria o css dinamico contendo as classes para inserção no style do componente
    let css = "";
    css += createAllDynamicClasses(props?.configTitle, configTitle, "configTitle");
    css += createAllDynamicClasses(props?.configText, configText, "configText");
    css += createDynamicClass("cssConfigDiv", configDiv.style);

    //Unifica todas as classes criadas para inserção no className da div
    //Classe padrão do sistema flex
    let cssDiv = "flex flex-column ";
    //Classes dinamicas criadas para inserção da cor caso haja
    cssDiv += (colorTitlePs + colorTextPs + colorDivPs.replace(";"," ")).replace(";", " ");
    //Classes pré configuradas pelo componente
    cssDiv += "cssConfigTitle cssConfigText cssConfigDiv";

    return (
        <div data-testid="div-test" className={cssDiv}>
            {props?.children}
            <style>
                {css}
            </style>
        </div>
    );
};

//Schema do componente
DivStyled.schema = DivStyleSchema;

export default DivStyled;