import { includeAllDefaultFonts, includeAllValues } from "../utils/util-div-styled";

//#region ColorList 

const DefaultColorList: string[] = [
    'indianred',
    'lightcoral',
    'salmon',
    'darksalmon',
    'lightsalmon',
    'crimson',
    'red',
    'firebrick',
    'darkred',
    'pink',
    'lightpink',
    'hotpink',
    'deeppink',
    'mediumvioletred',
    'palevioletred',
    'coral',
    'tomato',
    'orangered',
    'darkorange',
    'orange',
    'gold',
    'yellow',
    'lightyellow',
    'lemonchiffon',
    'lightgoldenrodyellow',
    'papayawhip',
    'moccasin',
    'peachpuff',
    'palegoldenrod',
    'khaki',
    'darkkhaki',
    'lavender',
    'thistle',
    'plum',
    'violet',
    'orchid',
    'fuchsia',
    'magenta',
    'mediumorchid',
    'mediumpurple',
    'blueviolet',
    'darkviolet',
    'darkorchid',
    'darkmagenta',
    'purple',
    'rebeccapurple',
    'indigo',
    'mediumslateblue',
    'slateblue',
    'darkslateblue',
    'greenyellow',
    'chartreuse',
    'lawngreen',
    'lime',
    'limegreen',
    'palegreen',
    'lightgreen',
    'mediumspringgreen',
    'springgreen',
    'mediumseagreen',
    'seagreen',
    'forestgreen',
    'green',
    'darkgreen',
    'yellowgreen',
    'olivedrab',
    'olive',
    'darkolivegreen',
    'mediumaquamarine',
    'darkseagreen',
    'lightseagreen',
    'darkcyan',
    'teal',
    'aqua',
    'cyan',
    'lightcyan',
    'paleturquoise',
    'aquamarine',
    'turquoise',
    'mediumturquoise',
    'darkturquoise',
    'cadetblue',
    'steelblue',
    'lightsteelblue',
    'powderblue',
    'lightblue',
    'skyblue',
    'lightskyblue',
    'deepskyblue',
    'dodgerblue',
    'cornflowerblue',
    'royalblue',
    'blue',
    'mediumblue',
    'darkblue',
    'navy',
    'midnightblue',
    'cornsilk',
    'blanchedalmond',
    'bisque',
    'navajowhite',
    'wheat',
    'burlywood',
    'tan',
    'rosybrown',
    'sandybrown',
    'goldenrod',
    'darkgoldenrod',
    'peru',
    'chocolate',
    'saddlebrown',
    'sienna',
    'brown',
    'maroon',
    'white',
    'snow',
    'honeydew',
    'mintcream',
    'azure',
    'aliceblue',
    'ghostwhite',
    'whitesmoke',
    'seashell',
    'beige',
    'oldlace',
    'floralwhite',
    'ivory',
    'antiquewhite',
    'linen',
    'lavenderblush',
    'mistyrose',
    'gainsboro',
    'lightgray',
    'lightgrey',
    'silver',
    'darkgray',
    'darkgrey',
    'gray',
    'grey',
    'dimgray',
    'dimgrey',
    'lightslategray',
    'lightslategrey',
    'slategray',
    'slategrey',
    'darkslategray',
    'darkslategrey',
    'black'
];

const PersonalizedColor: string[] = [ 
    'ps-brand-blue',
    'ps-brand-red',
    'ps-card-blue',
    'ps-red',
    'ps-orange',
    'ps-yellow',
    'ps-purple',
    'ps-green',
    'ps-dark-blue',
    'ps-bright-blue',
    'ps-light-blue',
    'ps-black',
    'ps-gray-1',
    'ps-gray-2',
    'ps-gray-3',
    'ps-gray-4',
    'ps-gray-5',
    'ps-white',
    'ps-back-brand-blue',
    'ps-back-brand-red',
    'ps-back-card-blue',
    'ps-back-red',
    'ps-back-orange',
    'ps-back-yellow',
    'ps-back-purple',
    'ps-back-green',
    'ps-back-dark-blue',
    'ps-back-bright-blue',
    'ps-back-light-blue',
    'ps-back-black',
    'ps-back-gray-1',
    'ps-back-gray-2',
    'ps-back-gray-3',
    'ps-back-gray-4',
    'ps-back-gray-5',
    'ps-back-white'
];

let ColorList: string[] = [''];

//Inclui a lista de cores padrões dentro da lista ColorList
//Old = ColorList            = Lista que serão incluidos os novos valores de cor
//NewList = DefaultColorList = Lista que será passada para incluir os novos valores dentro da lista antiga (Old) 
ColorList = includeAllValues(ColorList, DefaultColorList);

//Inclui a lista de personalizadas dentro da lista ColorList
//Essa lista contém classes pré definidas dentro do arquivo DivStyled.css
ColorList = includeAllValues(ColorList, PersonalizedColor);

export { ColorList };

//#endregion

//#region FontList

let FontList = [''];

//Gera uma lista de fontes padrões para que sejam selecionadas no Site Editor
//Old = FontList      - Lista que será utilizada para adicionar as novas fontes
//MinFont = 2         - Por qual número de fonte o for devera começar para gerar as fontes
//MaxFont = 100       - Número maximo da fonte que deverá ser incluida dentro das fontes pré definidas
//MultiPl = 2         - Multiplicador para determinar de quanto em quanto será contada a fonte. ex: [2px, 4px, 6px, 8px, 10px, ... 100px]
FontList = includeAllDefaultFonts(FontList);

export { FontList };

//#endregion

//#region TagsTitleList
//Adicionar outras tags de title aqui caso sejá necessário
const DefaultTagsTitleList: string[] = [
    "h1",
    "h2",
    "h3",
    "h4",
    "h5",
    "h6",
];

//Lista das tags titulo disponiveis para alteração por meio da div até o momento
let TagsTitleList = [''];
TagsTitleList = includeAllValues(TagsTitleList, DefaultTagsTitleList);

export { TagsTitleList };

//#endregion

//#region TagsTextList
//Adicionar outras tags de text aqui caso sejá necessário
const DefaultTagsTextList: string[] = [
    "div",
    "p",
    "span",
    "input",
    "form",
    "a",
    "ul",
    "li"
];

//Lista das tags de texto disponiveis para alteração por meio da div até o momento
let TagsTextList = [''];
TagsTextList = includeAllValues(TagsTextList, DefaultTagsTextList);

export { TagsTextList };
//#endregion