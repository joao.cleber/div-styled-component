import { ConfigDiv, ConfigText, ConfigTitle } from "../types/types-div-styled";

//Interface de Propriedades da Div
export interface DivProps {
    configTitle?: ConfigTitle[],
    configText?: ConfigText[],
    configDiv?: ConfigDiv
};

