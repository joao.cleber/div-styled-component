import { ColorList, FontList, TagsTextList, TagsTitleList } from "../enums/enum-div-styled";

export const DivStyleSchema = {
    title: "Div Styled",
    description: "Div Styled Component",
    type: "object",
    properties: {
        configDiv: {
            title: "Div Config",
            type: "object",
            properties: {
                isVisible: {
                    title: "Mostrar Componente",
                    type: "boolean",
                    default: true
                },
                colorDiv: {
                    title: "Background Color Div",
                    type: "string",
                    default: "",
                    enum: ColorList
                },
                marginDiv: {
                    title: "Margin Div",
                    type: "string",
                    default: ""
                },
                paddingDiv: {
                    title: "Padding Div",
                    type: "string",
                    default: ""
                },
                widthDiv: {
                    title: "Width Div",
                    type: "string",
                    default: ""
                },
                heightDiv: {
                    title: "Height Div",
                    type: "string",
                    default: ""
                }
            }
        },
        configTitle: {
            title: "Title Config",
            type: "array",
            items: {
                type: "object",
                properties: {                
                    tagTitle: {
                        title: "Tag Title",
                        type: "string",
                        default: "",
                        enum: TagsTitleList
                    },
                    colorTitle: {
                        title: "Color Title",
                        type: "string",
                        default: "",
                        enum: ColorList
                    },
                    sizeTitle: {
                        title: "Font-size Title",
                        type: "string",
                        default: "",
                        enum: FontList
                    },
                    paddingTitle: {
                        title: "Padding Title",
                        type: "string",
                        default: ""
                    }
                }
            }
        },
        configText: {
            title: "Text Config",
            type: "array",
            items: {
                type: "object",
                properties: {
                    tagText: {
                        title: "Tag Text",
                        type: "string",
                        default: "",
                        enum: TagsTextList
                    },
                    colorText: {
                        title: "Color Text",
                        type: "string",
                        default: "",
                        enum: ColorList
                    },
                    sizeText: {
                        title: "Font-size Text",
                        type: "string",
                        default: "",
                        enum: FontList
                    },
                    paddingText: {
                        title: "Padding Text",
                        type: "string",
                        default: ""
                    }
                }
            }
        }
    }
};
