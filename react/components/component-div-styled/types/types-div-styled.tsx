export type ConfigTitle = {
    tagTitle?: string, //Tag selecionada para o titulo
    colorTitle?: string, //Cor do título
    colorTitlePs?: boolean, //Verifica se a cor é personalizada ou não
    sizeTitle?: string, //Tamanho do título
    paddingTitle?: string //Padding do título. ex(20px 20px 50px 50px)
};

export type ConfigText = {
    tagText?: string, //Tag selecionada para o texto
    colorText?: string, //Cor do texto
    colorTextPs?: boolean, //Verifica se a cor é personalizada ou não
    sizeText?: string, //Tamanho do texto
    paddingText?: string //Padding do texto. ex(20px 20px 50px 50px)
};

export type ConfigDiv = {
    isVisible?: boolean, //Verifica se a div deve ser apresentada ou não
    colorDiv?: string, //Background color da div
    colorDivPs?: boolean, //Verifica se a cor do background é personalizada ou não
    marginDiv?: string, //Margem da div. ex(20px 20px 50px 50px)
    paddingDiv?: string, //Padding da div. ex(20px 20px 50px 50px)
    widthDiv?: string, //Largura da div
    heightDiv?: string //Altura da div
};