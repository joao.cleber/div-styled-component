import { ColorList } from "../enums/enum-div-styled";
import { ConfigDiv, ConfigText, ConfigTitle } from "../types/types-div-styled";
import style from '../DivStyled.css';

//Separa os parametros do configTitle passados pela prop para configurar os demais dados do componente
export function setConfigTitleStyle(props?: ConfigTitle[]): any {
    //Verifica se existem props informadas ou não, caso não exista retornara nulo
    if(props === undefined)
        return null;

    //Inicia as variaveis do loop
    let configTitle: any[] = [];
    let colorTitle: string = "";
    let vr_colorTitle: boolean;

    //Realiza o loop das props para configurar os styles passados
    props.forEach((element) => {
        //Seta as variaveis do loop para o padrão de cada loop
        colorTitle = "";
        vr_colorTitle = false;

        //Captura a cor definida caso haja
        colorTitle = setNaN(element?.colorTitle);
        //Faz a busca da cor informada a cima dentro a função de busca para confirmar que a cor realmente existe dentro do sistema
        vr_colorTitle = searchColor(colorTitle);
        colorTitle = vr_colorTitle ? colorTitle : "";

        //Seta as configurações no formato object verifica e se os parametros estão preenchidos ou não, e insere dentro do array de configuração
        configTitle.push({
            "style": {
                "color": colorTitle.includes("ps-") ? "" : colorTitle,
                "fontSize": setNaN(element?.sizeTitle),
                "padding": setNaN(element?.paddingTitle)
            },
            //Verifica se a cor informada é personalizada ou não
            "configTitlePs": colorTitle.includes("ps-")
        });
    });

    return configTitle;
};

//Separa os parametros do configText passados pela prop para configurar os demais dados do componente
export function setConfigTextStyle(props?: ConfigText[]): any {
    //Verifica se existem props informadas ou não, caso não exista retornara nulo
    if(props === undefined)
        return null;

    //Inicia as variaveis do loop
    let configText: any[] = [];
    let colorText: string = "";
    let vr_colorText: boolean;

    //Realiza o loop das props para configurar os styles passados
    props.forEach((element) => {
        //Seta as variaveis do loop para o padrão de cada loop
        colorText = "";
        vr_colorText = false;

        //Captura a cor definida caso haja
        colorText = setNaN(element?.colorText);
        //Faz a busca da cor informada a cima dentro a função de busca para confirmar que a cor realmente existe dentro do sistema
        vr_colorText = searchColor(colorText);
        colorText = vr_colorText ? colorText : "";

        //Seta as configurações no formato object verifica e se os parametros estão preenchidos ou não, e insere dentro do array de configuração
        configText.push({
            "style": {
                "color": colorText.includes("ps-") ? "" : colorText,
                "fontSize": setNaN(element?.sizeText),
                "padding": setNaN(element?.paddingText)
            },
            //Verifica se a cor informada é personalizada ou não
            "configTextPs": colorText.includes("ps-")
        });
    });

    return configText;
};

//Separa os parametros do configDiv passados pela prop para configurar os demais dados do componente
export function setConfigDivStyle(props?: ConfigDiv): any {
    //Pega a cor e verifica se o parametro passado está indefinido ou com alguma cor selecionada
    let colorDiv = setNaN(props?.colorDiv);
    //Faz a busca da cor informada a cima dentro a função de busca para confirmar que a cor realmente existe dentro do sistema
    const vr_colorDiv = searchColor(colorDiv);
    colorDiv = vr_colorDiv ? colorDiv : "";

    //Seta as configurações no formato object verifica se os parametros estão preenchidos ou não
    let configDiv = {
        "style": {
            "backgroundColor": colorDiv,
            "margin": setNaN(props?.marginDiv),
            "padding": setNaN(props?.paddingDiv),
            "width": setNaN(props?.widthDiv),
            "height": setNaN(props?.heightDiv)
        },
        //Verifica se a cor informada é personalizada ou não
        "configDivPs": colorDiv.includes("ps-back-")
    };

    //Caso a cor seja personalizada a cor sera definida para null pois o que será utilizado será a classe dentro do sistema e não uma cor expecifica
    configDiv.style.backgroundColor = configDiv.configDivPs ? "" : configDiv.style.backgroundColor;

    return configDiv;
};

//Verifica se os valores são undefined para que não ocorram problemas na leitura do código
export function setNaN(value: any): string {
    return value === undefined ? "" : value;
};

//Verifica se os valores são undefined para que não ocorram problemas na leitura do código porém insere '' (definida especificamente para o css)
export function setNaN2(value: any): string {
    return value === undefined || value === "" ? "''" : value;
};

//Faz a busca da cor selecionada para verificar se a mesma existe dentro do sistema
export function searchColor(color: string): boolean {
    return ColorList.includes(color);
};

//Função utilizada para preenchimento das listas onde é passado a lista padrão e os valores da outra lista que serão inseridas nela
//Inclui a lista de cores padrões dentro da lista ColorList
//Old = ColorList            = Lista que serão incluidos os novos valores de cor
//NewList = DefaultColorList = Lista que será passada para incluir os novos valores dentro da lista antiga (Old) 
export function includeAllValues(old: any, newList: any): any {
    newList.forEach((x: any) => {
        old.push(x);
    });

    return old;
};

//Função utilizada para gerar as fontes que serão utilizadas para alterar o tamanho dos textos e titulos
//Gera uma lista de fontes padrões para que sejam selecionadas no Site Editor
//Old = FontList      - Lista que será utilizada para adicionar as novas fontes
//MinFont = 2         - Por qual número de fonte o for devera começar para gerar as fontes
//MaxFont = 100       - Número maximo da fonte que deverá ser incluida dentro das fontes pré definidas
//MultiPl = 2         - Multiplicador para determinar de quanto em quanto será contada a fonte. ex: [2px, 4px, 6px, 8px, 10px, ... 100px]
export function includeAllDefaultFonts(old: any, minFont: any = 2, maxFont: any = 100, multiPl: number = 2): any {
    for(let i: number = minFont; i <= maxFont; i += multiPl) 
        old.push(i + "px"); 
    
    return old;
};

//Função utilizada para criar e retornar uma classe dinamica para inserção no style da div (apenas monta a classe com os valores passados)
export function createDynamicClass(name: string, styleCss: any): string {
    let css = "\n." + name;
    css += " {";
    css += "\n    color:"            + setNaN2(styleCss.color)            + ";";
    css += "\n    background-color:" + setNaN2(styleCss.backgroundColor)  + ";";
    css += "\n    padding:"          + setNaN2(styleCss.padding)          + ";";
    css += "\n    margin:"           + setNaN2(styleCss.margin)           + ";";
    css += "\n    width:"            + setNaN2(styleCss.width)            + ";";
    css += "\n    height:"           + setNaN2(styleCss.height)           + ";";
    css += "\n    font-size:"        + setNaN2(styleCss.fontSize)         + ";";
    css += "\n }";
    return css;
};

//Função utilizada para criar e retornar varias classes dinamicas para inserção no style da div (apenas monta a classe com os valores passados)
export function createAllDynamicClasses(props?: any[], styleCss?: any[], type?: string): string {
    if(styleCss === undefined || props === undefined)
        return "";

    let css: string = "";
    let i: number = 0;

    styleCss.forEach((element) => {

        let cssStyle: string = "";
        let verifyCss: boolean = false;

        //Verifica qual foi o tipo de config passada para setar um nome
        switch(type) {
            case "configTitle":
                cssStyle += "\n" + setNaN(props[i].tagTitle) === "" ? ".cssConfigTitle" : ".cssConfigTitle " + props[i].tagTitle;
                verifyCss = setNaN(props[i].tagTitle) === "" ? false :  true;
                i += 1;
                break;
            case "configText":
                cssStyle += "\n" + setNaN(props[i].tagText) === "" ? ".cssConfigText" : ".cssConfigText " + props[i].tagText;
                verifyCss = setNaN(props[i].tagText) === "" ? false :  true; 
                i += 1;
                break;
        };

        //Monta as propriedades da classe
        cssStyle += " {";
        cssStyle += "\n    color:"            + setNaN2(element.style.color)            + ";";
        cssStyle += "\n    background-color:" + setNaN2(element.style.backgroundColor)  + ";";
        cssStyle += "\n    padding:"          + setNaN2(element.style.padding)          + ";";
        cssStyle += "\n    margin:"           + setNaN2(element.style.margin)           + ";";
        cssStyle += "\n    width:"            + setNaN2(element.style.width)            + ";";
        cssStyle += "\n    height:"           + setNaN2(element.style.height)           + ";";
        cssStyle += "\n    font-size:"        + setNaN2(element.style.fontSize)         + ";";
        cssStyle += "\n }";

        if(verifyCss)
            css += "\n" + cssStyle;

    });

    return css;
};

//Função utilziada para verificar se a cor informada é personalizada ou não
export function setColorPsStyle(configs?: any[], type?: string, verify?: any[]): string {
    //Verifica se os dados passados são undefined para que retorne null
    if(configs === undefined || verify === undefined)
        return "";
    
    let value: string = "";

    //Verifica qual o tipo de configuração para estar verifica a classe
    switch(type) {
        case "configTitle":
            let i: number = 0;
            configs.forEach((element) => {
                value += verify[i] !== undefined && verify[i].configTitlePs ? style[setNaN(element.colorTitle)] + ";" : "";
                i += 1;
            });
            break;

        case "configText":
            configs.forEach((element) => {
                value += verify[i] !== undefined && verify[i].configTextPs ? style[setNaN(element.colorText)] + ";" : "";
            });
            break;
    };

    return value.replace(";", " ");
};