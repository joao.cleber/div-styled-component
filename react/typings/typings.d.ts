declare module "*.json" {
    const value: any;
    export default value;
};

declare module "*.jsonc" {
    const value: any;
    export default value;
};